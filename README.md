# Jogo Animais

Projeto utilizando AngularJS e Bootstrap para front-end, também foi utilizado http-server do npm para rodar o servidor para teste. O back-end foi utilizado JavaEE, Maven Project, e foi utilizado wildfly para rodar o servidor para teste.

A lógica do projeto é uma estrutura de dados do tipo árvore binária em que se tem um animal, e seu atributo base é sua descrição. Um animal se derivam dois objetos animais que podem ser FilhoSim e FilhoNão, a classe animal também possui atributos de pai.
Parte da lógica também se define em que quando um animal não tem filhos, automaticamente é uma resposta final, e isso irá trazer o fim do programa. Caso o programa acerte o animal, aparece uma mensagem de acerto, mas caso erre, um formulário aparece para o usuário preencher com o animal em que pensou e uma característica que difere do animal escolhido pela aplicação. 
Ao salvar esses dois dados enviados pelo usuário o back-end faz a lógica de achar o filho específico que deve ser substituído e faz todas as ações necessárias para isso.
