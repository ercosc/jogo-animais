app.controller('HomeCtrl', function ($scope, AnimalService, $window) {
    mostrar();

    //TANTO O ANIMAL RESPOSTA QUANTO DESCRIÇÃO RESPOSTA SÃO OBJETOS DO TIPO ANIMAL, ESTAREI TRATANDO DE 
    //FORMA DIFERENTE PARA FACILITAR A EXPLICAÇÃO DO CÓDIGO
    //cria no escopo o objeto Animal resposta que define a resposta do usuário quando o jogo erra o animal
    $scope.animalResposta = {
        descricao:"",
    };
    //cria no escopo o objeto de descrição para o animal em que o usuário define quando o jogo erra o animal
    $scope.descricaoResposta = {
        descricao: "",
        filhoSim: "",
        filhoNao: "",
    };

    //variável utilizada para trocar o escopo da página no fim do jogo
    $scope.isVisible = true;

    $scope.message = 'o animal que você pensou ';


    //função executada ao abrir a página. em que chama o animal service mostrar (o parametro 0 define o primeiro item do array no backend)
    function mostrar() {
        AnimalService.mostrar(0).then(function (animal) {
            return $scope.animal = animal;
        })
    }

    //função usada quando o usuário aperta o botão sim. 
    //verifica se o animal tem filho, caso tiver apresenta ele na tela caso não, determina o fim do jogo.
    $scope.getRespostaSim = function () {
        if($scope.animal.filhoSim) {
            $scope.animal = $scope.animal.filhoSim;
        } else {
            $scope.message = 'eu acertei :)'
            $scope.animal = '';
        }

    }

    //função usada quando o usuário aperta o botão não.
    //verifica se o animal tem filho, caso tiver apresenta na tela, caso não, determina o fim do jogo.
    $scope.getRespostaNao = function () {
        if($scope.animal.filhoNao) {
            $scope.animal = $scope.animal.filhoNao;
        } else {
            $scope.isVisible = !$scope.isVisible;
            $scope.message = 'qual animal você pensou?'
        }
    }

    //função usada quando o usuário aperta o botão reset. Atualiza a página
    $scope.reset = function() {
        $window.location.reload()
    }


    //função utilizada ao apertar o botão salvar
    $scope.salvar = function() {
        //primeiramente determina os filhos e o pai da descrição definida
        $scope.descricaoResposta.filhoSim = $scope.animalResposta;
        $scope.descricaoResposta.filhoNao = $scope.animal;
        $scope.descricaoResposta.pai = $scope.animal.pai;
        
        //invoca animal service para salvar passando o animal e a descrição a serem salvas
        AnimalService.salvar($scope.animalResposta, $scope.descricaoResposta);
        $window.location.href = "/";
    }
})