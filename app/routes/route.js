//seta todas as rotas da aplicação
app.config(function($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: "views/home.html",
        controller: 'HomeCtrl'
      })
      .otherwise({
      redirectTo: '/'
    });
  });