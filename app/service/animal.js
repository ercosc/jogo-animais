app.service('AnimalService', function($resource) {

    //seta o $resource para a aplicação backend em que eu particularmente estou utilizando.
    var api = $resource('http://localhost:8081/api/webresources/animais/:id', {}, {
        //utilizado para o método save poder receber objetos do tipo Array
        save: {
            method: 'POST',
            isArray: true
        }
    });

    //Função mostrar em que acessa o backend passando o id como parametro
    this.mostrar = function(id) {
        return api.get({ id: id }).$promise
    }

    //Função salvar em que recebe o animal e a descrição e passa os dois em formato de array para ser acessado
    //no backend
    this.salvar = function(animalResposta, descricaoResposta) {
        return api.save([animalResposta, descricaoResposta]);
    }
})