package animais;

import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Stateless
@Path("/animais")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
//aplicação de roteamento da aplicação utilizando padrões REST
public class AnimalService {
    
    private AnimalDao animalDao;
    
    //construtor vazio da classe em que instancia um animalDao
    public AnimalService() {
        animalDao = new AnimalDao();
    }
    
    
    @GET
    @Path("/{id}")
    //Requisição GET mostrar que recebe um id e segue o fluxo da programação em animalDao
    public Response Mostrar(@PathParam("id") Integer id) {
        return Response.ok(animalDao.mostrar(id)).build();
    }
    
    @POST
    //Requisição POST salvar que recebe um array de animais(os dois resposta e descrição) e segue o fluxo da programação em animalDao
    public Response salvar(ArrayList<Animal> animais) {
        return Response.ok(animalDao.salvar(animais)).build();
    }
}
