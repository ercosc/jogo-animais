package animais;

import java.util.ArrayList;
import javax.inject.Singleton;


interface IAnimalDao {
    
    @Singleton
    SimulatedDatabase sdb = new SimulatedDatabase();
    
    public Animal mostrar(Integer id);
    
    public ArrayList<Animal> salvar(ArrayList<Animal> animais);
}
