package animais;

import java.util.ArrayList;

public class SimulatedDatabase {

    //array de animais em que ficarão salvos os animais na base de dados simulada
    private ArrayList<Animal> animais;
    //contador que será utilizado para controle de Id
    Integer contador;

    //construtor da base de dados em que inicializa as variáveis e adiciona os
    //os 3 animais iniciais para a aplicação conseguir rodar (uma pergunta e duas respostas)
    public SimulatedDatabase() {
        this.contador = 0;
        this.animais = new ArrayList<>();
        animais.add(new Animal(contador++, "vive na água"));
        animais.add(new Animal(contador++, "tubarao", 0));
        animais.add(new Animal(contador++, "macaco", 0));
        this.setFilhosInit(mostrar(0));
    }

    //seta os filhos para os animais iniciais da aplicação
    private void setFilhosInit(Animal animal) {
        animal.setFilhoSim(mostrar(1));
        animal.setFilhoNao(mostrar(2));
    }

    //método em que recebe um id e percorre a lista de animais, e retorna o animal com o id indicado
    //o método retornará null caso não encontre o id expecífico
    public Animal mostrar(Integer id) {
        for (Animal a : animais) {
            if (a.getId().equals(id)) {
                return a;
            }
        }
        return null;
    }

    //método onde se seta os novos filhos e pais quando um objeto é adicionado
    //seta um novo filho para o objeto pai e um novo pai para o objeto filho
    private void setFilhos(Animal pai, Animal filho) {
        //verificação para caso da resposta não
        if (pai.getFilhoNao().getId() == filho.getFilhoNao().getId()) {
            pai.getFilhoNao().setPai(filho.getId());
            pai.setFilhoNao(filho);
        }
        //verificação para resposta sim
        if (pai.getFilhoSim().getId() == filho.getFilhoNao().getId()) {
            pai.getFilhoSim().setPai(filho.getId());
            pai.setFilhoSim(filho);
        }
    }

    //método salvar em que recebe um arraylist de animais e os adiciona na lista da base de dados
    public ArrayList<Animal> salvar(ArrayList<Animal> animais) {
        Animal a1 = animais.get(0);
        Animal a2 = animais.get(1);

        this.adicionarAnimal(a2);
        this.adicionarAnimal(a1);

        //objeto a1 sempre será o filho e o a2 sempre o pai
        //aqui chama o método em que adiciona um pai para o objeto filho
        this.setPai(a1, a2.getId());
        //aqui chama o método em que se seta os filhos para os objetos que já existem
        this.setFilhos(mostrar(a2.getPai()), a2);
        return this.animais;
    }

    //método em que se seta um pai para o objeto animal filho
    private void setPai(Animal filho, Integer pai) {
        filho.setPai(pai);
    }

    //método em que se adiciona um novo animal a lista da base de dados simulada
    //seta um id para o animal que será adicionado e automaticamente já se itera o contador para futuras inserções
    private void adicionarAnimal(Animal animal) {
        animal.setId(contador++);
        animais.add(animal);
    }

}
