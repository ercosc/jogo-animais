/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package animais;

import java.util.ArrayList;
import javax.inject.Singleton;


public class AnimalDao implements IAnimalDao {
    
    @Singleton
    //Base de dados simulada singleton para não ser instanciada multiplas vezes
    private SimulatedDatabase sdb;
    
    //construtor da classe AnimalDao
    public AnimalDao() {
        this.sdb = new SimulatedDatabase();
    }
    

    @Override
    //método que sobrepoe a interface em que recebe um id e acessa a base de dados simulada para retornar esse objeto
    public Animal mostrar(Integer id) {
        return sdb.mostrar(id);
    }

    @Override
    //método que sobrepoe a interface em que recebe os dois animais e acessa a base de dados para salvar esses dois objetos
    public ArrayList<Animal> salvar(ArrayList<Animal> animais) {
        return sdb.salvar(animais);
    }
}
