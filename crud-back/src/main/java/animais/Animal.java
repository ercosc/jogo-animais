package animais;

//classe modelo para Animal
public final class Animal {

    private Integer id;
    private String descricao;
    private Animal filhoSim;
    private Animal filhoNao;
    private Integer pai;
    
    public Animal() {
        
    }
    
    public Animal(Integer id, String descricao, Integer pai) {
        this.setId(id);
        this.setDescricao(descricao);
        this.setPai(pai);
    }

    public Animal(Integer id, String descricao, Animal filhoSim, Animal filhoNao) {
        this.setId(id);
        this.setDescricao(descricao);
        this.setFilhoSim(filhoSim);
        this.setFilhoNao(filhoNao);
    }

    public Animal(Integer id, String descricao) {
        this.setId(id);
        this.setDescricao(descricao);
    }
    
    public Integer getId() {
        return this.id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    public Animal getFilhoSim() {
        return this.filhoSim;
    }
    public void setFilhoSim(Animal r) {
        this.filhoSim = r;
    }
    
    public Animal getFilhoNao(){
        return this.filhoNao;
    }
    public void setFilhoNao(Animal r) {
        this.filhoNao = r;
    }
    
    public Integer getPai() {
        return this.pai;
    }
    public void setPai(Integer pai) {
        this.pai = pai;
    }
    
    @Override
    public String toString() {
        return this.getId() + " " + this.getDescricao() + " " + this.getFilhoSim().getDescricao() + " " + this.getFilhoNao().getDescricao() + " " + this.getPai();
    }
}
